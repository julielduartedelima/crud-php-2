-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 31-Jan-2023 às 06:47
-- Versão do servidor: 10.4.27-MariaDB
-- versão do PHP: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `juliel_phpcrud`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `task`
--

CREATE TABLE `task` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Extraindo dados da tabela `task`
--

INSERT INTO `task` (`id`, `title`, `description`, `created_at`) VALUES
(1, 'Nome:', 'Juliel Duarte de Lima', '2023-01-31 14:55:22'),
(2, 'Objetivo:', 'Conseguir uma oportunidade de emprego para trabalhar em sua empresa como programador júnior ou analista de sistemas júnior.', '2023-01-08 14:55:22'),
(3, 'Contato:', '(21) 99602-8509', '2023-01-31 14:55:22'),
(4, 'e-mail:', 'julielduartedelima@gmail.com', '2023-01-31 14:55:22'),
(5, 'Formação Acadêmica:', 'Pós graduação em engenharia de software com métodos ágeis - Universidade Cruzeiro do Sul. (concluído em 2022)<br><br>Tecnólogo em Análise e Desenvolvimento de sistemas - Universidade Estácio de Sá. (concluído em 2021)', '2023-01-08 14:55:22'),
(6, 'Idioma:', 'Inglês - Intermediário', '2023-01-31 14:55:22'),
(7, 'Competências', 'Desenvolvimento web com java, PHP, modelagem de banco de dados, infra estrutura de redes TCP/IP, administração de servidores, projetos de redes e automação de redes.', '2023-01-08 14:55:22'),
(8, 'Experiência:', 'Trabalhei na empresa Alves e Duarte Tecnologia e Sistemas Ltda, que fica no bairro Glória - RJ, no ano de 2020 a 2021, como programador júnior.', '2023-01-08 14:55:22'),
(9, 'Endereço:', 'Rua Barão de Guaratiba, 59, quarto 14, Glória, Rio de Janeiro - RJ.', '2023-01-08 14:55:22'),
(15, 'Explicação:', 'Fiz esse sistema web usando PHP, banco de dados MySQL e Bootstrap para mostrar as informações do meu currículo e demonstrar que sei programar.', '2023-01-08 14:55:22');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `task`
--
ALTER TABLE `task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
